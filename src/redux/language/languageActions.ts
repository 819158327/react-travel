export const CHANGE_LANGUAGE = "change_language";
export const ADD_LANGUAGE = "add_language";

// 创建更改语言的Action
interface ChangeLanguageAction {
    type: typeof CHANGE_LANGUAGE,
    payload: "zh" | "en"
}

export const changeLanguageActionCreator = (languageCode: "zh" | "en"): ChangeLanguageAction => {
    return {
        type: CHANGE_LANGUAGE,
        payload: languageCode
    };
};

// 添加新语言的Action
interface AddLanguageAction {
    type: typeof ADD_LANGUAGE,
    payload: { name: string, code: string }
}

export const addLanguageActionCreator = (name: string, code: string): AddLanguageAction => {
    return {
        type: ADD_LANGUAGE,
        payload: {name: name, code: code}
    };
};

export type LanguageActionTypes = ChangeLanguageAction | AddLanguageAction
