/**
 * 首页推荐产品数据
 * */
// 爆款推荐
export const productList1 = [
    {
        id: 1,
        title:
            "埃及阿斯旺+卢克索+红海Red Sea+开罗+亚历山大12日跟团游(5钻)·【官方旗舰明星纯玩团】25人封顶|含签证小费全程餐|3晚尼罗河游轮+3晚红海全包度假村+1晚底比斯古都|升级内陆飞机|优质中文导游队伍|七大神庙+赠项目",
        price: "11990",
        touristRoutePictures: [
            {
                url: "https://img1.baidu.com/it/u=2235856219,1421761082&fm=26&fmt=auto",
            },
        ],
    },
    {
        id: 2,
        title: "摩洛哥撒哈拉沙漠+卡萨布兰卡+马拉喀什+舍夫沙...",
        price: "13290",
        touristRoutePictures: [
            {
                url: "https://img0.baidu.com/it/u=1171196108,2898383934&fm=26&fmt=auto",
            },
        ],
    },
    {
        id: 3,
        title: "越南胡志明市+美奈+芽庄+河内7日6晚跟团游(4钻)...",
        price: "4000",
        touristRoutePictures: [
            {
                url: "https://img2.baidu.com/it/u=3819590483,2275108492&fm=26&fmt=auto",
            },
        ],
    },
    {
        id: 4,
        title: "迪拜+阿布扎比6日跟团游(5钻)·【携程国旅纯玩...",
        price: "7399",
        touristRoutePictures: [
            {
                url: "https://img1.baidu.com/it/u=1638670601,2976246805&fm=26&fmt=auto",
            },
        ],
    },
    {
        id: 5,
        title: "泰国曼谷+芭堤雅6日5晚跟团游(5钻)·【纯玩】『可...",
        price: "3499",
        touristRoutePictures: [
            {
                url: "https://img2.baidu.com/it/u=1978159485,689704831&fm=26&fmt=auto",
            },
        ],
    },
    {
        id: 6,
        title: "日本大阪+京都+箱根+东京6日5晚跟团游(4钻)·【浪...",
        price: "5999",
        touristRoutePictures: [
            {
                url: "https://img1.baidu.com/it/u=275475408,4194259033&fm=26&fmt=auto",
            },
        ],
    },
    {
        id: 7,
        title: "新加坡+马来西亚6日5晚跟团游(5钻)·【纯玩无购物...",
        price: "6199",
        touristRoutePictures: [
            {
                url: "https://img0.baidu.com/it/u=2426199942,3201213013&fm=26&fmt=auto",
            },
        ],
    },
    {
        id: 8,
        title: "法国+德国+意大利+瑞士12日跟团游(4钻)·【匠心定...",
        price: "13699",
        touristRoutePictures: [
            {
                url: "https://img2.baidu.com/it/u=304548428,4118808455&fm=26&fmt=auto",
            },
        ],
    },
    {
        id: 9,
        title: "印度尼西亚巴厘岛7日5晚私家团(5钻)·A线独栋泳...",
        price: "5021",
        touristRoutePictures: [
            {
                url: "https://img0.baidu.com/it/u=731923128,3874789940&fm=26&fmt=auto",
            },
        ],
    },
];

// 新品上市
export const productList2 = [
    {
        id: 10,
        title:
            "南京3日2晚跟团游(4钻)·观中山陵+游总统府+览博物院『游六朝古都 听漫长历史』&逛秦淮河风光带+老门东『品美食 唤醒您的舌尖』&牛首山+报恩寺『诚心祈福 放空心灵』& 2晚连住4钻酒店",
        price: "11990",
        touristRoutePictures: [
            {
                url: "https://img2.baidu.com/it/u=1818849091,931154052&fm=26&fmt=auto",
            },
        ],
    },
    {
        id: 11,
        title: "摩洛哥撒哈拉沙漠+卡萨布兰卡+马拉喀什+舍夫沙...",
        price: "13290",
        touristRoutePictures: [
            {
                url: "https://img2.baidu.com/it/u=2096359682,3358613328&fm=26&fmt=auto",
            },
        ],
    },
    {
        id: 12,
        title: "越南胡志明市+美奈+芽庄+河内7日6晚跟团游(4钻)...",
        price: "4000",
        touristRoutePictures: [
            {
                url: "https://img1.baidu.com/it/u=1261798046,3584677176&fm=26&fmt=auto",
            },
        ],
    },
    {
        id: 13,
        title: "迪拜+阿布扎比6日跟团游(5钻)·【携程国旅纯玩...",
        price: "7399",
        touristRoutePictures: [
            {
                url: "https://img2.baidu.com/it/u=3600612670,1607535642&fm=26&fmt=auto",
            },
        ],
    },
    {
        id: 14,
        title: "泰国曼谷+芭堤雅6日5晚跟团游(5钻)·【纯玩】『可...",
        price: "3499",
        touristRoutePictures: [
            {
                url: "https://img0.baidu.com/it/u=4114565881,3140879167&fm=26&fmt=auto",
            },
        ],
    },
    {
        id: 15,
        title: "日本大阪+京都+箱根+东京6日5晚跟团游(4钻)·【浪...",
        price: "5999",
        touristRoutePictures: [
            {
                url: "https://img0.baidu.com/it/u=30791093,3071666792&fm=26&fmt=auto",
            },
        ],
    },
    {
        id: 16,
        title: "新加坡+马来西亚6日5晚跟团游(5钻)·【纯玩无购物...",
        price: "6199",
        touristRoutePictures: [
            {
                url: "https://img2.baidu.com/it/u=2719756519,2976596655&fm=26&fmt=auto",
            },
        ],
    },
    {
        id: 17,
        title: "法国+德国+意大利+瑞士12日跟团游(4钻)·【匠心定...",
        price: "13699",
        touristRoutePictures: [
            {
                url: "https://img1.baidu.com/it/u=1799188466,3943369421&fm=26&fmt=auto",
            },
        ],
    },
    {
        id: 18,
        title: "印度尼西亚巴厘岛7日5晚私家团(5钻)·A线独栋泳...",
        price: "5021",
        touristRoutePictures: [
            {
                url: "https://img1.baidu.com/it/u=1679581246,36414568&fm=26&fmt=auto",
            },
        ],
    },
];

// 国内游推荐
export const productList3 = [
    {
        id: 19,
        title:
            "埃及阿斯旺+卢克索+红海Red Sea+开罗+亚历山大12日跟团游(5钻)·【官方旗舰明星纯玩团】25人封顶|含签证小费全程餐|3晚尼罗...",
        price: "11990",
        touristRoutePictures: [
            {
                url: "https://img0.baidu.com/it/u=987872638,1276077342&fm=26&fmt=auto",
            },
        ],
    },
    {
        id: 20,
        title: "摩洛哥撒哈拉沙漠+卡萨布兰卡+马拉喀什+舍夫沙...",
        price: "13290",
        touristRoutePictures: [
            {
                url: "https://img0.baidu.com/it/u=1809102721,3822444334&fm=26&fmt=auto",
            },
        ],
    },
    {
        id: 21,
        title: "越南胡志明市+美奈+芽庄+河内7日6晚跟团游(4钻)...",
        price: "4000",
        touristRoutePictures: [
            {
                url: "https://img1.baidu.com/it/u=1679581246,36414568&fm=26&fmt=auto",
            },
        ],
    },
    {
        id: 22,
        title: "迪拜+阿布扎比6日跟团游(5钻)·【携程国旅纯玩...",
        price: "7399",
        touristRoutePictures: [
            {
                url: "https://img0.baidu.com/it/u=1254062960,1193012058&fm=26&fmt=auto",
            },
        ],
    },
    {
        id: 23,
        title: "泰国曼谷+芭堤雅6日5晚跟团游(5钻)·【纯玩】『可...",
        price: "3499",
        touristRoutePictures: [
            {
                url: "https://img1.baidu.com/it/u=2267057457,1256050141&fm=26&fmt=auto",
            },
        ],
    },
    {
        id: 24,
        title: "日本大阪+京都+箱根+东京6日5晚跟团游(4钻)·【浪...",
        price: "5999",
        touristRoutePictures: [
            {
                url: "https://img1.baidu.com/it/u=3055777780,3331962984&fm=26&fmt=auto",
            },
        ],
    },
    {
        id: 25,
        title: "新加坡+马来西亚6日5晚跟团游(5钻)·【纯玩无购物...",
        price: "6199",
        touristRoutePictures: [
            {
                url: "https://img1.baidu.com/it/u=3764731555,3006629475&fm=26&fmt=auto",
            },
        ],
    },
    {
        id: 26,
        title: "法国+德国+意大利+瑞士12日跟团游(4钻)·【匠心定...",
        price: "13699",
        touristRoutePictures: [
            {
                url: "https://img0.baidu.com/it/u=1067630210,474090807&fm=26&fmt=auto",
            },
        ],
    },
    {
        id: 27,
        title: "印度尼西亚巴厘岛7日5晚私家团(5钻)·A线独栋泳...",
        price: "5021",
        touristRoutePictures: [
            {
                url: "https://img2.baidu.com/it/u=571466718,597800879&fm=26&fmt=auto",
            },
        ],
    },
];
