import React from "react";
import {Image, Typography} from "antd"
import {withRouter, RouteComponentProps, Link} from "react-router-dom"


interface PropsType extends RouteComponentProps {
    id: string | number;
    size: "large" | "small";
    imageSrc: string;
    price: string | number;
    title: string;
}

const ProductImageComponent: React.FC<PropsType> = ({
                                                        id, size, imageSrc, price, title,
                                                        history, location, match
                                                    }) => {
    return (
        <Link target={"_blank"} to={`detail/${id}`}>
            {
                size === "large" ? (<Image preview={false} src={imageSrc} width={490} height={290}/>)
                    : (<Image preview={false} src={imageSrc} width={240} height={120}/>)
            }
            <div>
                <Typography.Text type={"secondary"}>{title.slice(0, 25)}</Typography.Text>
                <Typography.Text type={"danger"} strong={true}>￥{price}起</Typography.Text>
            </div>
        </Link>
    );
}

export const ProductImage = withRouter(ProductImageComponent)
