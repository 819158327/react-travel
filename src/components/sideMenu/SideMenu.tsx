import React from "react";
import styles from "./SideMenu.module.css"
import {sideMenuList} from "./mockup";
import {Menu} from "antd";
import {GifOutlined} from "@ant-design/icons"


export const SideMenu: React.FC = () => {
    return (
        <Menu mode={"vertical"} className={styles.sileMenu}>
            {sideMenuList.map((item, index) => (
                <Menu.SubMenu key={`parent-menu-${index}`}
                              title={<span><GifOutlined/>{item.title}</span>}>
                    {
                        item.subMenu.map((child2, index2) => (
                            <Menu.SubMenu key={`child2-menu-${index2}`}
                                          title={<span><GifOutlined/>{child2.title}</span>}>
                                {
                                    child2.subMenu.map((child3, index3) => (
                                        <Menu.Item key={`child3-menu-${index3}`}
                                                   title={<span><GifOutlined/>{child3}</span>}/>
                                    ))
                                }
                            </Menu.SubMenu>
                        ))
                    }
                </Menu.SubMenu>
            ))}
        </Menu>
    );
}
