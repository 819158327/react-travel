import React from "react";
import {Layout} from "antd";
import styles from "../../App.module.css";
import Title from "antd/es/typography/Title";
import {Footer} from "antd/es/layout/layout";
import {useTranslation} from "react-i18next";


export const Footers: React.FC = () => {

    const {t} = useTranslation();

    return (
        <Layout>
            <Footer className={styles.footerContainerStyle}>
                <Title level={3} style={{textAlign: "center"}}>{t("footer.detail")}</Title>
            </Footer>
        </Layout>
    );
}
