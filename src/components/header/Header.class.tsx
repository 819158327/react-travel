import React from "react";
import styles from "./Header.module.css"
import Text from "antd/es/typography/Text";
import {Button, Dropdown, Input, Layout, Menu} from "antd";
import {Header} from "antd/es/layout/layout";
import {GlobalOutlined} from "@ant-design/icons";
import logo from "../../assets/logo.svg";
import Title from "antd/es/typography/Title";
import {withRouter, RouteComponentProps} from "react-router-dom"
import {RootState} from "../../redux/store";
import {withTranslation, WithTranslation} from "react-i18next";
import {changeLanguageActionCreator, addLanguageActionCreator} from "../../redux/language/languageActions";
import {connect} from "react-redux";
import {Dispatch} from "redux";


const mapStateToProps = (state: RootState) => {
    return {
        language: state.language,
        languageList: state.languageList
    }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
    return {
        changeLanguage: (code: "zh" | "en") => {
            const action = changeLanguageActionCreator(code);
            dispatch(action)
        },
        addLanguage: (name: string, code: string) => {
            const action = addLanguageActionCreator(name, code);
            dispatch(action);
        }
    }
}

class HeadersComponent extends React.Component<RouteComponentProps & WithTranslation
    & ReturnType<typeof mapStateToProps>
    & ReturnType<typeof mapDispatchToProps>> {

    // 处理点击事件
    handlerMenuClick = (e) => {
        if (e.key === "new") {
            this.props.addLanguage("新语言", "new_language")
        } else {
            this.props.changeLanguage(e.key)
        }
    }

    render() {
        const {history, t} = this.props;
        return (
            <Layout>
                <Header className={styles.topHeaderContainerStyle}>
                    <Text>{t("header.slogan")}</Text>
                    <Dropdown.Button style={{marginLeft: 10}}
                                     overlay={
                                         <Menu onClick={this.handlerMenuClick}>
                                             {
                                                 this.props.languageList.map(l => {
                                                     return (<Menu.Item key={l.code}>{l.name}</Menu.Item>)
                                                 })
                                             }
                                             <Menu.Item key={"new"}>{t("header.add_new_language")}</Menu.Item>
                                         </Menu>
                                     }
                                     icon={<GlobalOutlined/>}>
                        {this.props.language === "zh" ? "中文" : "English"}
                    </Dropdown.Button>
                    <div className={styles.placeHolderViewStyle}/>
                    <Button.Group className={styles.buttonGroupStyle}>
                        <Button onClick={() => {
                            history.push("register")
                        }}>{t("header.register")}</Button>
                        <Button onClick={() => {
                            history.push("signIn")
                        }}>{t("header.signin")}</Button>
                    </Button.Group>
                </Header>
                <Header className={styles.headerContainerStyle}>
                    <img src={logo} alt={''} className={styles['App-logo']} onClick={() => {
                        history.push("/")
                    }}/>
                    <Title level={3} className={styles.titleStyle} onClick={() => {
                        history.push("/")
                    }}>{t("header.title")}</Title>
                    <Input.Search placeholder={'请输入旅游目的地、主题或关键字'} className={styles.searchInputStyle}/>
                </Header>
                <Menu mode={"horizontal"} className={styles.topMenuStyle}>
                    <Menu.Item key={1}>{t("header.home_page")}</Menu.Item>
                    <Menu.Item key={2}>{t("header.weekend")}</Menu.Item>
                    <Menu.Item key={3}>{t("header.group")}</Menu.Item>
                    <Menu.Item key={4}>{t("header.backpack")}</Menu.Item>
                    <Menu.Item key={5}>{t("header.private")}</Menu.Item>
                    <Menu.Item key={6}>{t("header.cruise")}</Menu.Item>
                    <Menu.Item key={7}>{t("header.hotel")}</Menu.Item>
                    <Menu.Item key={8}>{t("header.local")}</Menu.Item>
                    <Menu.Item key={9}>{t("header.theme")}</Menu.Item>
                    <Menu.Item key={10}>{t("header.custom")}</Menu.Item>
                    <Menu.Item key={11}>{t("header.study")}</Menu.Item>
                    <Menu.Item key={12}>{t("header.visa")}</Menu.Item>
                    <Menu.Item key={13}>{t("header.enterprise")}</Menu.Item>
                    <Menu.Item key={14}>{t("header.high_end")}</Menu.Item>
                    <Menu.Item key={15}>{t("header.outdoor")}</Menu.Item>
                    <Menu.Item key={16}>{t("header.insurance")}</Menu.Item>
                </Menu>
            </Layout>
        );
    }
}

export const Headers = connect(mapStateToProps, mapDispatchToProps)(withTranslation()(withRouter(HeadersComponent)));
