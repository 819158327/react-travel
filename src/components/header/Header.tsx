import React from "react";
import styles from "./Header.module.css"
import Text from "antd/es/typography/Text";
import {Button, Dropdown, Input, Layout, Menu} from "antd";
import {Header} from "antd/es/layout/layout";
import {GlobalOutlined} from "@ant-design/icons";
import logo from "../../assets/logo.svg";
import Title from "antd/es/typography/Title";
import {useHistory, useLocation, useParams, useRouteMatch} from "react-router-dom"
import {useSelector} from "../../redux/hooks";
import {useDispatch} from "react-redux";
import {
    LanguageActionTypes,
    addLanguageActionCreator,
    changeLanguageActionCreator
} from "../../redux/language/languageActions";
import {useTranslation} from "react-i18next";

export const Headers: React.FC = () => {

    const history = useHistory();
    const location = useLocation();
    const params = useParams();
    const routeMatch = useRouteMatch();

    const language = useSelector((state) => state.language)
    const languageList = useSelector((state) => state.languageList)

    const dispatch = useDispatch();
    // const dispatch = useDispatch<Dispatch<LanguageActionTypes>>()

    const {t} = useTranslation();

    // 处理点击事件
    const handlerMenuClick = (e) => {
        if (e.key === "new") {
            dispatch(addLanguageActionCreator("新语言", "new_language"))
        } else {
            dispatch(changeLanguageActionCreator(e.key))
        }
    }

    return (
        <Layout>
            <Header className={styles.topHeaderContainerStyle}>
                <Text>{t("header.slogan")}</Text>
                <Dropdown.Button style={{marginLeft: 10}}
                                 overlay={
                                     <Menu onClick={handlerMenuClick}>
                                         {
                                             languageList.map(l => {
                                                 return (<Menu.Item key={l.code}>{l.name}</Menu.Item>)
                                             })
                                         }
                                         <Menu.Item key={"new"}>{t("header.add_new_language")}</Menu.Item>
                                     </Menu>
                                 }
                                 icon={<GlobalOutlined/>}>
                    {language === "zh" ? "中文" : "English"}
                </Dropdown.Button>
                <div className={styles.placeHolderViewStyle}/>
                <Button.Group className={styles.buttonGroupStyle}>
                    <Button onClick={() => {
                        history.push("register")
                    }}>{t("header.register")}</Button>
                    <Button onClick={() => {
                        history.push("signIn")
                    }}>{t("header.signin")}</Button>
                </Button.Group>
            </Header>
            <Header className={styles.headerContainerStyle}>
                <img src={logo} alt={''} className={styles['App-logo']} onClick={() => {
                    history.push("/")
                }}/>
                <Title level={3} className={styles.titleStyle} onClick={() => {
                    history.push("/")
                }}>{t("header.title")}</Title>
                <Input.Search placeholder={'请输入旅游目的地、主题或关键字'} className={styles.searchInputStyle}/>
            </Header>
            <Menu mode={"horizontal"} className={styles.topMenuStyle}>
                <Menu.Item key={1}>{t("header.home_page")}</Menu.Item>
                <Menu.Item key={2}>{t("header.weekend")}</Menu.Item>
                <Menu.Item key={3}>{t("header.group")}</Menu.Item>
                <Menu.Item key={4}>{t("header.backpack")}</Menu.Item>
                <Menu.Item key={5}>{t("header.private")}</Menu.Item>
                <Menu.Item key={6}>{t("header.cruise")}</Menu.Item>
                <Menu.Item key={7}>{t("header.hotel")}</Menu.Item>
                <Menu.Item key={8}>{t("header.local")}</Menu.Item>
                <Menu.Item key={9}>{t("header.theme")}</Menu.Item>
                <Menu.Item key={10}>{t("header.custom")}</Menu.Item>
                <Menu.Item key={11}>{t("header.study")}</Menu.Item>
                <Menu.Item key={12}>{t("header.visa")}</Menu.Item>
                <Menu.Item key={13}>{t("header.enterprise")}</Menu.Item>
                <Menu.Item key={14}>{t("header.high_end")}</Menu.Item>
                <Menu.Item key={15}>{t("header.outdoor")}</Menu.Item>
                <Menu.Item key={16}>{t("header.insurance")}</Menu.Item>
            </Menu>
        </Layout>
    );
}
