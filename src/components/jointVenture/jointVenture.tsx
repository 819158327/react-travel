import React from "react";
import {Col, Divider, Layout, Row, Typography} from "antd";

import image1 from "../../assets/images/facebook-807588_640.png"
import image2 from "../../assets/images/follow-826033_640.png"
import image3 from "../../assets/images/icon-720944_640.png"
import image4 from "../../assets/images/microsoft-80658_640.png"

const data = [
    {src: image1, title: "facebook"},
    {src: image2, title: "follow"},
    {src: image3, title: "youtube"},
    {src: image4, title: "microsoft"}
]

export const JointVenture: React.FC = () => {
    return (
        <Layout style={{backgroundColor: "#eeeeee", marginTop: "20px", marginBottom: "20px"}}>
            <Divider orientation={"left"}>
                <Typography.Title level={3}>合作企业</Typography.Title>
            </Divider>
            <Row>
                {
                    data.map((item, index) => (
                        <Col span={6} key={`joint-venture-${index}`}>
                            <img src={item.src} alt={item.title}
                                 style={{
                                     width: "80%",
                                     marginLeft: "auto",
                                     marginRight: "auto",
                                     display: "block"
                                 }}/>
                        </Col>
                    ))
                }
            </Row>
        </Layout>
    );
}
